﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;

namespace TestBuilder.Helpers
{
    public class ResponseHelper
    {
        public ResponseHelper()
        {

        }
        public void SetResponseValue(HttpResponseMessage response, dynamic inputContent )
        {
            string outputConent = JsonConvert.SerializeObject(inputContent, Formatting.Indented, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });

            response.Content = new StringContent(outputConent, Encoding.UTF8, "application/json");
            response.StatusCode = HttpStatusCode.OK;
        }
    }
}