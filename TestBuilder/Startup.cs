﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Owin;

using Owin;
using TestBuilder.Data;
using System.Web.Http;


[assembly: OwinStartup(typeof(TestBuilder.Startup))]

namespace TestBuilder
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            HttpConfiguration config = new HttpConfiguration();

            WebApiConfig.Register(config);
            app.UseWebApi(config);
            DataContext context = new DataContext();
            context.Database.Initialize(true);
        }
    }
}
