﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestBuilder.Models.EntityModels
{
    public class User
    {
        public int Id { get; set; }

        public string UserName { get; set; }

        public string Email { get; set; }

        public int FacultyId { get; set; }

        public DateTime CreatedDate { get; set; }

        public int CreatedBy { get; set; }
    }
}