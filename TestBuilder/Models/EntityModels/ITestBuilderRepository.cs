﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestBuilder.Models.EntityModels
{
    interface ITestBuilderRepository
    {
        IQueryable<Question> GetQuestions();
        IQueryable<Faculty> GetFaculties();
    }
}
