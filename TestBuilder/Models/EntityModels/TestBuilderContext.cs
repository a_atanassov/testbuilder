﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace TestBuilder.Models.EntityModels
{
    public class TestBuilderContext : DbContext
    {
        public TestBuilderContext()
            : base("TestBuilderDB")
        {

        }

        public DbSet<User> Users { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<QuestionType> QuestionTypes { get; set; }
        public DbSet<Faculty> Faculties { get; set; }

    }
}