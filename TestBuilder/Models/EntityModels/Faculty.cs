﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestBuilder.Models.EntityModels
{
    public class Faculty
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public DateTime CreatedDate { get; set; }

        public int CreatedBy { get; set; }
    }
}