﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Http.Cors;
using TestBuilder.Data;
using TestBuilder.Data.EntityModels;
using TestBuilder.Data.POCOModels;
using TestBuilder.Data.Repositories;

namespace TestBuilder.Controllers
{
    [EnableCorsAttribute("*", "*", "*")]
    public class FacultyController : ApiController
    {
        private UnitOfWork unitOfWOrk;
        public FacultyController()
        {
            this.unitOfWOrk = new UnitOfWork();
        }
      
        [HttpGet]
        [Route("api/faculty/getAllFaculties")]
        public HttpResponseMessage GetAllFaculties()
        {
            HttpResponseMessage response = Request.CreateResponse();
            try
            {
                List<Faculty> faculties = this.unitOfWOrk.Faculties.GetAll().ToList();
                string content = JsonConvert.SerializeObject(faculties, Formatting.Indented, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });

                response.Content = new StringContent(content, Encoding.UTF8, "application/json");
                response.StatusCode = HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                
                throw;
            }

            return response;



        }

        [HttpPost]
        [Route("api/faculty/createFaculty")]
        public HttpResponseMessage CreateFaculty(BasicModel faculty)
        {
            HttpResponseMessage response = Request.CreateResponse();

            try
            {
                if (ModelState.IsValid)
                {
                    this.unitOfWOrk.Faculties.CreateFaculty(faculty);

                    response.StatusCode = HttpStatusCode.Created;
                }
                else
                {
                    response.StatusCode = HttpStatusCode.BadRequest;
                }
            }
            catch (Exception ex)
            {
                response.StatusCode = HttpStatusCode.BadRequest;
                throw;
            }

            return response;
        }

    }
}
