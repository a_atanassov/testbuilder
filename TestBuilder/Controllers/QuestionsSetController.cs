﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Http.Cors;
using TestBuilder.Data;
using TestBuilder.Data.EntityModels;
using TestBuilder.Data.POCOModels;
using TestBuilder.Data.Repositories;
using TestBuilder.Helpers;

namespace TestBuilder.Controllers
{
    [EnableCorsAttribute("*", "*", "*")]
    public class QuestionsSetController : ApiController
    {

        private UnitOfWork unitOfWork;
        ResponseHelper helper;

        public QuestionsSetController()
        {
            this.unitOfWork = new UnitOfWork();
            helper = new ResponseHelper();

        }

        [HttpGet]
        [Route("api/questionSet/GetAllQuestionSetsForFaculty/{facultyId}")]
        public HttpResponseMessage GetAllQuestionSetsForFaculty(int facultyId)
        {
            HttpResponseMessage response = Request.CreateResponse();

            IEnumerable<QuestionSetModel> questionnaires = this.unitOfWork.QuestionSets.GetAllQuestionsSetsForFaculty(facultyId);
            string content = JsonConvert.SerializeObject(questionnaires, Formatting.Indented, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });

            response.Content = new StringContent(content, Encoding.UTF8, "application/json");
            response.StatusCode = HttpStatusCode.OK;


            return response;
        }

        [HttpGet]
        [Route("api/questionSet/GetSubjects")]
        public HttpResponseMessage GetSubjects()
        {
            HttpResponseMessage response = Request.CreateResponse();
            try
            {
                IEnumerable<BasicModel> subjects = this.unitOfWork.Subjects.GetAllSubjects();
                helper.SetResponseValue(response, subjects);
            }
            catch (Exception ex)
            {
                response.StatusCode = HttpStatusCode.BadRequest;
                throw;
            }

            return response;
        }

        [HttpGet]
        [Route("api/questionSet/GetQuestionSet/{questionSetId}")]
        public HttpResponseMessage GetQuestionSet(int questionSetId)
        {
            HttpResponseMessage response = Request.CreateResponse();
            try
            {
                QuestionSetBasicModel result = this.unitOfWork.QuestionSets.GetQuestionSetForEdit(questionSetId);
                helper.SetResponseValue(response, result);

                response.StatusCode = HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                
                throw;
            }

            return response;
        }

        [HttpPost]
        [Route("api/questionSet/CreateQuestionSet")]
        public HttpResponseMessage CreateQuestion(QuestionSetBasicModel givenQuestion)
        {
            HttpResponseMessage response = Request.CreateResponse();

            try
            {
                this.unitOfWork.QuestionSets.CreateQuestion(givenQuestion, this.unitOfWork);

                response.StatusCode = HttpStatusCode.Created;
            }
            catch (Exception ex)
            {
                
                throw;
            }

            return response;
        }

        [HttpPost]
        [Route("api/questionSet/UpdateQuestionSet")]
        public HttpResponseMessage UpdateQuestionSet(QuestionSetBasicModel givenQuestion)
        {
            HttpResponseMessage response = Request.CreateResponse();

            try
            {
                this.unitOfWork.QuestionSets.UpdateQuestion(givenQuestion, this.unitOfWork);

                response.StatusCode = HttpStatusCode.Created;
            }
            catch (Exception ex)
            {

                throw;
            }

            return response;
        }


    }
}
