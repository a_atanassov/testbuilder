﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Http.Cors;
using TestBuilder.Data;
using TestBuilder.Data.POCOModels;
using TestBuilder.Data.Repositories;
using TestBuilder.Helpers;

namespace TestBuilder.Controllers
{
    [EnableCorsAttribute("*", "*", "*")]
    public class QuestionsController : ApiController
    {
        private UnitOfWork unitOfWork;
        private ResponseHelper responseHelper;

        public QuestionsController()
        {
            this.unitOfWork = new UnitOfWork();
            responseHelper = new ResponseHelper();
        }

        [HttpGet]
        [Route("api/questions/GetExistingQuestions/{facultyId}/{subjectId}")]
        public HttpResponseMessage GetExistingQuestions(int facultyId, int subjectId)
        {
            HttpResponseMessage response = Request.CreateResponse();
            try
            {
                IEnumerable<QuestionBasicModel> questions = this.unitOfWork.Questions.GetExistingQuestionsForFacultyAndSubject(facultyId, subjectId);
                responseHelper.SetResponseValue(response, questions);

            }
            catch (Exception)
            {
                response.StatusCode = HttpStatusCode.BadRequest;
                throw;
            }
            

            return response;
        }

    }
}
