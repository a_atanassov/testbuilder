﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestBuilder.Data.EntityModels;
using TestBuilder.Data.POCOModels;

namespace TestBuilder.Data.Repositories
{
    public class AnswerRepository : BaseRepository<Answer>
    {
        public AnswerRepository(DbContext dbContext)
            : base(dbContext)
        {

        }

        public void CreateAnswer(AnswerBasicModel answerToCreate, Question answersQuestion, int sequence)
        {
            Answer answerToAdd = new Answer();
            answerToAdd.Name = answerToCreate.Name;
            answerToAdd.IsCorrect = answerToCreate.IsCorrect;
            answerToAdd.Question = answersQuestion;
            answerToAdd.Sequence = sequence;

            this.Add(answerToAdd);

        }
    }
}
