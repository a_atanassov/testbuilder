﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestBuilder.Data.EntityModels;
using TestBuilder.Data.POCOModels;

namespace TestBuilder.Data.Repositories
{
    public class FacultyRepository : BaseRepository<Faculty>
    {
        public FacultyRepository(DbContext dbContext)
            : base(dbContext)
        {

        }

        public void CreateFaculty(BasicModel faculty)
        {
            Faculty facultyToAdd = new Faculty()
            {
                Name = faculty.Name
            };
            try
            {
                this.Add(facultyToAdd);
                this.Context.SaveChanges();

            }
            catch (Exception ex)
            {
                
                throw;
            }
        }
    }
}
