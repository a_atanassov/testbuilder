﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestBuilder.Data.EntityModels;
using TestBuilder.Data.POCOModels;
using System.Data.Entity;

namespace TestBuilder.Data.Repositories
{
    public class QuestionRepository : BaseRepository<Question>
    {
        public QuestionRepository(DbContext dbContext)
            : base(dbContext)
        {

        }

        public IEnumerable<QuestionSetModel> GetAllQuestionsForQuestionSet(int questionSetId)
        {
            return this.DbSet.Where(q => q.QuestionSetId == questionSetId)
                .Select(q => new QuestionSetModel() { Id = q.Id, Name = q.Name });
        }

        public IEnumerable<QuestionBasicModel> GetExistingQuestionsForFacultyAndSubject(int facultyId, int subjectId)
        {
            return this.DbSet.Include(q => q.QuestionsSet).Where(q => q.QuestionsSet.FacultyId == facultyId
                        && q.QuestionsSet.SubjectId == subjectId).OrderBy(q => q.Sequence)
                        .Select(q => new QuestionBasicModel()
                        {
                            //Id = q.Id,
                            Name = q.Name,
                            TypeId = q.QuestionTypeId,
                            Answers = q.Answers.OrderBy(a => a.Sequence).Select
                            (a => new AnswerBasicModel() { Id = a.Id, Name = a.Name, IsCorrect = a.IsCorrect }).ToList()
                        }).ToList();
        }

        
    }
}
