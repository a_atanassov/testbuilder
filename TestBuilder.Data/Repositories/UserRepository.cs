﻿//using System;
//using System.Collections.Generic;
//using System.Data.Entity;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using TestBuilder.Data.EntityModels;
//using Microsoft.AspNet.Identity;
//using Microsoft.AspNet.Identity.EntityFramework;

//namespace TestBuilder.Data.Repositories
//{
//    public class UserRepository : BaseRepository<User>
//    {
//        //private DbContext _context;
//        private UserManager<IdentityUser> _userManager;
//        public UserRepository(DbContext dbContext)
//            : base(dbContext)
//        {
//            _userManager = new UserManager<IdentityUser>(new UserStore<IdentityUser>(this.unitOfWork.DbContext));
//        }

//        public async Task<IdentityResult> RegisterUser(User userModel)
//        {
//            IdentityUser user = new IdentityUser
//            {
//                UserName = userModel.UserName
//            };

//            var result = await _userManager.CreateAsync(user, userModel.Password);

//            return result;
//        }

//        public async Task<IdentityUser> FindUser(string userName, string password)
//        {
//            IdentityUser user = await _userManager.FindAsync(userName, password);

//            return user;
//        }
//    }
//}
