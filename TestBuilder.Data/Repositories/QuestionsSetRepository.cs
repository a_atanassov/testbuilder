﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestBuilder.Data.EntityModels;
using TestBuilder.Data.POCOModels;

namespace TestBuilder.Data.Repositories
{
    public class QuestionsSetRepository : BaseRepository<QuestionsSet>
    {
        public QuestionsSetRepository(DbContext dbContext)
            : base(dbContext)
        {

        }

        public IEnumerable<QuestionSetModel> GetAllQuestionsSetsForFaculty(int facultyId)
        {
            return this.DbSet.Where(q => q.FacultyId == facultyId)
                .Select(q => new QuestionSetModel() { Id = q.Id, Name = q.Name , Subject = q.Subject.Name }).ToList();
        }

        public void CreateQuestion(QuestionSetBasicModel givenQuestion, UnitOfWork unitOfWork)
        {
            QuestionsSet questionSetToCreate = new QuestionsSet();
            questionSetToCreate.Name = givenQuestion.Name;
            questionSetToCreate.FacultyId = givenQuestion.FacultyId;
            questionSetToCreate.SubjectId = givenQuestion.SubjectId;
            questionSetToCreate.Questions = new List<Question>();

            int questionSequence = 1;
            int answerSequence = 1;
            Question questionToAdd = null;

            foreach (QuestionBasicModel question in givenQuestion.Questions)
            {
                questionToAdd = new Question();
                if (question.Id != 0)
                {
                    questionToAdd = unitOfWork.Questions.GetById(question.Id);
                    questionToAdd.Sequence = questionSequence;
                    questionToAdd.QuestionsSet = questionSetToCreate;
                    questionSetToCreate.Questions.Add(questionToAdd);
                }

                else
                {
                    questionToAdd.Name = question.Name;
                    questionToAdd.QuestionsSet = questionSetToCreate;
                    questionToAdd.QuestionTypeId = question.TypeId;
                    questionToAdd.Sequence = questionSequence;
                    foreach (AnswerBasicModel item in question.Answers)
                    {
                        unitOfWork.Answers.CreateAnswer(item, questionToAdd, answerSequence);
                        answerSequence++;
                    }

                    questionSetToCreate.Questions.Add(questionToAdd);
                }
                questionSequence++;
            }
            this.Add(questionSetToCreate);
           
            unitOfWork.Commit();

        }

        public void UpdateQuestion(QuestionSetBasicModel givenQuestion, UnitOfWork unitOfWork)
        {
            QuestionsSet questionSetToUpdate = this.DbSet.Find(givenQuestion.Id);
            if (questionSetToUpdate == null)
            {
                throw new ArgumentNullException("No such question set found");
            }
            
            questionSetToUpdate.Name = givenQuestion.Name;
            questionSetToUpdate.FacultyId = givenQuestion.FacultyId;
            questionSetToUpdate.SubjectId = givenQuestion.SubjectId;

            //get questions and check to delete 
            List<int> alreadyPresentQuestionsId = givenQuestion.Questions.Where(q => q.Id != 0).Select(q => q.Id).ToList();

            List<Question> questionsToDelete = questionSetToUpdate.Questions.ToList();

            questionsToDelete.RemoveAll(q => alreadyPresentQuestionsId.Contains(q.Id));

            unitOfWork.Questions.DeleteRange(questionsToDelete);

            

            int questionSequence = 1;
            int answerSequence = 1;
            Question questionToAdd = null;

            foreach (QuestionBasicModel question in givenQuestion.Questions)
            {
                if (question.Id != 0)
                {
                    Question questionToUpdate = unitOfWork.Questions.GetById(question.Id);
                    questionToUpdate.Sequence = questionSequence;
                    questionToUpdate.Name = question.Name;
                    //answers not editable

                    unitOfWork.Questions.Update(questionToUpdate);
                }

                else
                {
                    questionToAdd = new Question();
                    questionToAdd.Name = question.Name;
                    questionToAdd.QuestionsSet = questionSetToUpdate;
                    questionToAdd.QuestionTypeId = question.TypeId;
                    questionToAdd.Sequence = questionSequence;
                    foreach (AnswerBasicModel item in question.Answers)
                    {
                        unitOfWork.Answers.CreateAnswer(item, questionToAdd, answerSequence);
                        answerSequence++;
                    }
                    questionSetToUpdate.Questions.Add(questionToAdd);

                }
                questionSequence++;
            }

            this.Update(questionSetToUpdate);

            unitOfWork.Commit();
        }

        public QuestionSetBasicModel GetQuestionSetForEdit(int questionSetId)
        {
            QuestionSetBasicModel modelToReturn = new QuestionSetBasicModel();

            QuestionsSet questionSetToReturn = this.DbSet.Find(questionSetId);
            if (questionSetToReturn == null)
            {
                throw new ArgumentNullException("No such question set found");
            }

            modelToReturn.Name = questionSetToReturn.Name;
            modelToReturn.Id = questionSetToReturn.Id;
            modelToReturn.FacultyId = questionSetToReturn.FacultyId;
            modelToReturn.SubjectId = questionSetToReturn.SubjectId;

            modelToReturn.Questions = questionSetToReturn.Questions.OrderBy(q => q.Sequence)
                                        .Select(questionToAdd => new QuestionBasicModel()
                                        {
                                            Id = questionToAdd.Id,
                                            Name = questionToAdd.Name,
                                            QuestionSetId = questionSetId,
                                            TypeId = questionToAdd.QuestionTypeId,
                                            Answers = questionToAdd.Answers.OrderBy(a => a.Sequence)
                                                        .Select(a => new AnswerBasicModel() 
                                                        { Id = a.Id, Name = a.Name, IsCorrect = a.IsCorrect }).ToList()
                                        }).ToList();

            return modelToReturn;
 
            
        }
    }
}
