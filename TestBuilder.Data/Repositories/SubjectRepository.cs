﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestBuilder.Data.EntityModels;
using TestBuilder.Data.POCOModels;

namespace TestBuilder.Data.Repositories
{
    public class SubjectRepository : BaseRepository<Subject>
    {
        public SubjectRepository(DbContext dbContext)
            : base(dbContext)
        {

        }

        public IEnumerable<BasicModel> GetAllSubjects()
        {
            return this.DbSet.Select(s => new BasicModel() { Id = s.Id, Name = s.Name }).ToList();
        }
    }
}
