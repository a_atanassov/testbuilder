﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestBuilder.Data.EntityModels;

namespace TestBuilder.Data
{
    public interface IUnitOfWork
    {
        void Commit();
        DataContext DbContext { get; }

      
    }
}
