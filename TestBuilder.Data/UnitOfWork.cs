﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestBuilder.Data.EntityModels;
using TestBuilder.Data.Repositories;

namespace TestBuilder.Data
{
    public class UnitOfWork : IDisposable
    {
        public DataContext DbContext { get; private set; }

        public FacultyRepository Faculties { get; private set; }

        public QuestionRepository Questions { get; private set; }

        public QuestionsSetRepository QuestionSets { get; private set; }

        public AnswerRepository Answers { get; private set; }

        public SubjectRepository Subjects { get; private set; }


        public UnitOfWork()
        {
            CreateDbContext();
            Faculties = new FacultyRepository(DbContext);
            Questions = new QuestionRepository(DbContext);
            QuestionSets = new QuestionsSetRepository(DbContext);
            Answers = new AnswerRepository(DbContext);
            Subjects = new SubjectRepository(DbContext);
        }

        protected void CreateDbContext()
        {
            DbContext = new DataContext();
        }


        public void Commit()
        {
            DbContext.SaveChanges();
        }
        #region IDisposable

        public void Dispose()
        {
            if (DbContext != null)
            {
                DbContext.Dispose();
            }
        }

        #endregion

    }
}
