﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using TestBuilder.Data.EntityModels;

namespace TestBuilder.Data.Configurations
{
    public class QuestionConfiguration : EntityTypeConfiguration<Question> 
    {
        public QuestionConfiguration()
        {
            this.Property(p => p.QuestionTypeId).IsRequired();

            this.Property(p => p.Name).IsRequired().HasMaxLength(500);
        }
    }
}
