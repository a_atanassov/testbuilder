﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data;
using TestBuilder.Data.EntityModels;
namespace TestBuilder.Data.Configurations
{
    public class CustomDatabaseInitializer :
        DropCreateDatabaseIfModelChanges<DataContext>
        //DropCreateDatabaseAlways<DataContext>
        //CreateDatabaseIfNotExists<DataContext>
    {
        protected override void Seed(DataContext context)
        {
            Faculty faculty = new Faculty()
            {
                Name = "Faculty one"
            };
            Faculty facultyTwo = new Faculty()
            {
                Name = "Faculty two"
            };

            List<Subject> subjects = new List<Subject>() 
            {
                new Subject(){Id = 1, Name = "Math"},
                new Subject(){Id = 2,Name = "Physics"},
                new Subject(){Id = 3,Name = "German"}
            };

            List<QuestionType> questionTypes = new List<QuestionType>() 
            {
                new QuestionType(){Id = 1,Name = "Single choice", },
                new QuestionType(){Id = 2, Name = "Multiple choice"},
                new QuestionType(){Id = 3, Name = "Free text"},

            };

            List<Answer> answers = new List<Answer>() 
            {
                new Answer(){Id = 1, Name= "Test answer one", QuestionId = 1, Sequence = 1, IsCorrect = true},
                new Answer(){Id = 2, Name= "Test answer two", QuestionId = 2, Sequence = 1, IsCorrect = true},
                new Answer(){Id = 3, Name= "Test answer three", QuestionId = 3, Sequence = 1, IsCorrect = true}
 
            };

            List<QuestionsSet> questionSets = new List<QuestionsSet>() 
            {
                new QuestionsSet(){Id = 1, Name = "Test question set one", SubjectId = 1, FacultyId = 1},
                new QuestionsSet(){Id = 2, Name = "Test question set two", SubjectId = 2, FacultyId = 2}
            };

            List<Question> questions = new List<Question>() 
            {
                new Question(){Id = 1,Name = "The first question", QuestionTypeId = 1, QuestionSetId = 1, Sequence = 1 },
                new Question(){Id = 2,Name = "The second question", QuestionTypeId = 2, QuestionSetId = 1, Sequence = 2} ,
                new Question(){Id = 3,Name = "The third question", QuestionTypeId = 3, QuestionSetId = 2, Sequence = 1 },


            };

        //Answers = new List<Answer>(){new Answer(){Id = 1, Name= "Test answer one",}}
        // Answers = new List<Answer>(){new Answer(){Id = 2, Name= "Test answer one",}
        //Answers = new List<Answer>(){new Answer(){Id = 3, Name= "Test answer one",}}

            context.Faculties.Add(faculty);
            context.Faculties.Add(facultyTwo);
            context.QuestionTypes.AddRange(questionTypes);
            context.Subjects.AddRange(subjects);
            context.Questions.AddRange(questions);
            context.Answers.AddRange(answers);
            context.QuestionSets.AddRange(questionSets);
           


            base.Seed(context);
        }
    }
}
