﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestBuilder.Data.EntityModels;

namespace TestBuilder.Data.POCOModels
{
    public class QuestionBasicModel : BasicModel
    {
        public List<AnswerBasicModel> Answers { get; set; }

        public int TypeId { get; set; }

        public int QuestionSetId { get; set; }

    }
}
