﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestBuilder.Data.POCOModels
{
    public class AnswerBasicModel : BasicModel
    {
        public bool IsCorrect { get; set; }
    }
}
