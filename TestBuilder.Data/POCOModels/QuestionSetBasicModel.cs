﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestBuilder.Data.POCOModels
{
    public class QuestionSetBasicModel : BasicModel
    {
        public int SubjectId { get; set; }

        public int FacultyId { get; set; }

        public List<QuestionBasicModel> Questions { get; set; }
    }
}
