﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TestBuilder.Data.EntityModels
{
    public class Question : IAuditInfo
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [MaxLength(150)]
        public string Name { get; set; }

        public int QuestionTypeId { get; set; }

        public int QuestionSetId { get; set; }

        [Required]
        public int Sequence { get; set; }

        public DateTime CreatedOn { get; set; }

        public DateTime ModifiedOn { get; set; }

        [ForeignKey("QuestionSetId")]
        public virtual QuestionsSet QuestionsSet { get; set; }

        [ForeignKey("QuestionTypeId")]
        public QuestionType QuestionType { get; set; }

        //[Required]
        public virtual ICollection<Answer> Answers { get; set; }
    }
}