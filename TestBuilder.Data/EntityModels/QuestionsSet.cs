﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestBuilder.Data.EntityModels
{
    public class QuestionsSet : IAuditInfo
    {
        [Key]
        public int Id { get; set; }

        [MaxLength(500)]
        public string Name { get; set; }

        public int SubjectId { get; set; }

        [ForeignKey("SubjectId")]
        public Subject Subject { get; set; }
        
        public int FacultyId { get; set; }

        [ForeignKey("FacultyId")]
        public Faculty Faculty { get; set; }

        public DateTime CreatedOn { get; set; }

        public DateTime ModifiedOn { get; set; }

        public virtual ICollection<Question> Questions { get; set; }
    }
}
