﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestBuilder.Data.EntityModels
{
    public class Subject : IAuditInfo
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public DateTime CreatedOn { get; set; }

        public DateTime ModifiedOn { get; set; }
    }
}
