﻿testBuilderApp.controller('studentsController',
    function studentsController( $scope, studentsService, $rootScope, $location) {
        //$scope.student = studentsService.student;

        $scope.studentsForm = {};

        $scope.student = {};
        $scope.studentsArray = [];

        $scope.true = true;
        $scope.false = false;

        $scope.student.form = {};
        $scope.student.form.name = "Select form of training";

        $scope.student.reason = {};
        $scope.student.reason.name = "Select reason";

        $scope.student.faculty = {};
        $scope.student.faculty.name = "Select faculty";

       

        $scope.student.department = {};
        $scope.student.department.name = "Select department";

        $scope.reasons = studentsService.reasons;
        $scope.formOfTraining = studentsService.formOfTraining;
        $scope.faculties = studentsService.faculties;

        $scope.selectFormOftraining = function (form) {
            $scope.student.form = angular.copy(form);
        }

        $scope.selectFaculty = function (faculty) {
            $scope.student.faculty = angular.copy(faculty);
        }

        $scope.selectDepartment = function (department) {
            $scope.student.department = angular.copy(department);
        }

        $scope.selectReason = function (reason) {
            $scope.student.reason = angular.copy(reason);
        }

        $scope.addStudent = function () {
            if ($scope.studentsForm.$valid && $scope.student.reason.id && $scope.student.form.id &&
                 $scope.student.faculty.id ) {
                if (!$rootScope.students) {
                    $rootScope.students = [];
                    $rootScope.students.push($scope.student);
                    $location.path('/');
                }
            }
            else {
                $scope.showInvalid = true;
            }
          
        }

        $scope.removeStudent = function (student) {
            var index = $rootScope.students.indexOf(student);
            if (typeof (index) != 'undefined') {
                $rootScope.students.splice(index, 1);

            }
        }



    })