﻿testBuilderApp.factory('studentsService',
    function () {
        var studentsServiceFactory = {};
       
        var formOfTraining = [
            { id: 1, name: 'Regular training' },
            { id: 2, name: 'Distance training' }

        ];

        var reasons = [
            { id: 1, name: 'Public' },
            { id: 2, name: 'Paid training' },
            { id: 3, name: 'In agreement' },
            { id: 4, name: 'Free of charge' },
            { id: 5, name: 'By Decree of the Council of Ministers '},
            { id: 6, name: 'Transferred from other universities' },
            { id: 7, name: 'Student exchange / Erasmus / '},
            { id: 8, name: 'Other' },


        ]

        var faculties = [
            {
                id: 1, name: 'FAGIOPM', departments: [
                   {
                       id: 1, name: 'Test department 1'
                   },
                   {id: 2, name: 'Test department 2'}

                ]
            },
            { id: 2, name: 'CSTU', departments: [{id: 3, name: 'Random department'}] },
            { id: 3, name: 'Test faculty', departments: [] },

        ];

        studentsServiceFactory.formOfTraining = formOfTraining;
        studentsServiceFactory.reasons = reasons;
        studentsServiceFactory.faculties = faculties;

        return studentsServiceFactory;
    })