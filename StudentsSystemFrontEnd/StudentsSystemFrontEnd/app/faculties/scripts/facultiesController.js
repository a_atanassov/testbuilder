﻿testBuilderApp.controller('facultiesController', ['$scope','ngDialog',
    function facultiesController($scope, ngDialog) {
        $scope.faculties = [{ name: "Test one" }, { name: "Test two" }, { name: "Test three" }];

        $scope.createFaculty = function () {

            var dialogObj = {
                template: 'app/faculties/views/createFacultyTemplate.html',
                className: 'ngdialog-theme-default',
                scope: $scope,
                closeByDocument: false,
                //controller: ['$scope', function ($scope) {
                //    $scope.todelete = element;

                //}]
            }
            ngDialog.open(dialogObj);
            //ngDialog.open({ template: 'app/faculties/views/createFacultyTemplate.html' });
        }
    }]);