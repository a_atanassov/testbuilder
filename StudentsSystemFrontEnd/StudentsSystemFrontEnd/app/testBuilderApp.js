﻿var testBuilderApp = angular.module('testBuilderApp', ['ngResource', 'ngRoute', 'ngTable', 'ngDialog'])
    .config(function ($routeProvider) {
        $routeProvider.when('/createStudent',
            {
                templateUrl: 'app/templates/studentsTemplate.html',
                controller: 'studentsController'
            })
            .when('/',
            {
                templateUrl: 'app/templates/homeTemplate.html',
                controller: 'studentsController'
            })
            .when('/students',
            {
                templateUrl: 'app/templates/studentsTable.html',
                controller: 'studentsController'
            })
            .when('/faculties',
            {
                templateUrl: 'app/faculties/views/manageFaculties.html',
                controller: 'facultiesController'
            })
            .when('/questions',
            {
                templateUrl: 'app/questions/views/manageQuestions.html',
                controller: 'questionsController'
            })
        
    });