﻿testBuilderApp.controller('userManagementController', ['$scope',
    function userManagementController($scope) {
        $scope.user = { name: '', password: '' };

        $scope.isUserLoggedIn = false;

        $scope.logIn = function () {
            $scope.isUserLoggedIn = true;

        }
    }]);