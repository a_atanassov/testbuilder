﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestBuilder.Data;

namespace TestBuilder.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Initializing Database...");

            DataContext context = new DataContext();
            context.Database.Initialize(true);

            Console.WriteLine("Done..");
            Console.ReadLine();
        }
    }
}
