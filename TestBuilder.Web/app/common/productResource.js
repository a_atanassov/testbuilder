﻿testBuilderApp.factory('productResource', ['$http', '$q', function ($http, $q) {

    var productResourceFactory = {};
    productResourceFactory.serviceBase = "http://localhost:45753/";

    var _deferredResourceGet = function (resourceURL) {
        var deferred = $q.defer();

        $http.get(resourceURL, {
            //headers: {
            //    'TimeZoneID': timeZoneID
            //}
        }).success(function (response) {
            deferred.resolve(response);
        }).error(function (err, status) {
            //authService.logOut();
            deferred.reject(err);
        });

        return deferred.promise;
    }

    var _deferredResourcePost = function (resourceUrl, data) {
        var deferred = $q.defer();

        $http.defaults.headers.common['Content-Type'] = 'application/json';

        $http.post(resourceUrl, data, {
           
        }).success(function (response) {
            deferred.resolve(response);
        }).error(function (err, status) {
            if (status === 401) {
                logOut('unauthorised');
            }
            deferred.reject({ err: err, status: status });
        });


        return deferred.promise;
    };

    productResourceFactory.defferedResourceGet = _deferredResourceGet;
    productResourceFactory.defferedResourcePost = _deferredResourcePost;

    return productResourceFactory;
}])