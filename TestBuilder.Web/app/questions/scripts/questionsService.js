﻿testBuilderApp.factory('questionsService', ['$http', '$q', 'productResource', function ($http, $q, productResource) {
    var questionsFactory = {};
    var serviceBase = productResource.serviceBase;


    var _getAllQuestionSetsForFaculty = function (facultyId) {
        var resourceURL = serviceBase + 'api/questionSet/GetAllQuestionSetsForFaculty/' + facultyId;
        var promise = productResource.defferedResourceGet(resourceURL);

        return promise;
    }

    var _getAllSubjects = function () {
        var resourceURL = serviceBase + 'api//questionSet/GetSubjects';
        var promise = productResource.defferedResourceGet(resourceURL);

        return promise;
    }

    var _getExistingQuestions = function (facultyId, subjectId) {
        var resourceURL = serviceBase + 'api/questions/GetExistingQuestions/' + facultyId + '/' + subjectId;
        var promise = productResource.defferedResourceGet(resourceURL);

        return promise;
    }

    var _createQuestionSet = function (questionSet) {
        var resourceURL = serviceBase + 'api/questionSet/CreateQuestionSet';
        var promise = productResource.defferedResourcePost(resourceURL, questionSet);

        return promise;

    }

    var _updateQuestionSet = function (questionSet) {
        var resourceURL = serviceBase + 'api/questionSet/UpdateQuestionSet';
        var promise = productResource.defferedResourcePost(resourceURL, questionSet);

        return promise;

    }

    var _getExistingQuestionSet = function (questionSetId) {
        var resourceURL = serviceBase + 'api/questionSet/GetQuestionSet/' + questionSetId;
        var promise = productResource.defferedResourceGet(resourceURL);

        return promise;
    }

    questionsFactory.getAllQuestionSetsForFaculty = _getAllQuestionSetsForFaculty;
    questionsFactory.getAllSubjects = _getAllSubjects;
    questionsFactory.getExistingQuestions = _getExistingQuestions;
    questionsFactory.createQuestionSet = _createQuestionSet;
    questionsFactory.updateQuestionSet = _updateQuestionSet;
    questionsFactory.getExistingQuestionSet = _getExistingQuestionSet;

    return questionsFactory;
}])
