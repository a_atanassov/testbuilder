﻿testBuilderApp.controller('questionPreviewController', ['$scope', 'questionsService', '$location',  'ngDialog', '$routeParams'
    , function questionPreviewController($scope, questionsService, $location,ngDialog, $routeParams) {
        $scope.questionSet = {};
        $scope.questionSetId = $routeParams.questionSetId;

        var init = function () {
            questionsService.getExistingQuestionSet($scope.questionSetId).then(function (data) {
                $scope.questionSet = data;
            })
        }

        init();

        $scope.checkCorrectAnswers = function () {

        }
    }]);
        