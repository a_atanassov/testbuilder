﻿testBuilderApp.controller('questionsController', ['$scope', '$filter', 'questionsService', 'ngTableParams', 'facultiesServices', '$location',
    function questionsController($scope, $filter, questionsService, ngTableParams, facultiesServices, $location) {
        $scope.questionSets = [];
        $scope.currentQuestionSet = {};
        $scope.selectedFaculty = {};
        $scope.searchQuestionSets = '';

        $scope.faculties = {};

        var init = function () {
            facultiesServices.getAllFaculties().then(function (faculties) {
                $scope.faculties = faculties;
            });

        }

        init();

        $scope.selectFaculty = function (faculty) {
            $scope.selectedFaculty = faculty;

            questionsService.getAllQuestionSetsForFaculty($scope.selectedFaculty.id).then(function (data) {
                $scope.questionSets = data;

            });
        }


        $scope.$watch("searchQuestionSets", function () {
            $scope.tableParams.filter().name = $scope.searchQuestionSets;
            $scope.tableParams.reload();
            $scope.tableParams.page(1);
        });

        $scope.$watch("questionSets", function () {
            $scope.tableParams.reload();
            $scope.tableParams.page(1);
        }, true);

        $scope.tableParams = new ngTableParams({
            page: 1,
            total: $scope.questionSets.length,
            count: 10,
            filter: {
                name: $scope.searchQuestionSets,
                
            },
            sorting: { name: 'asc' }
        },
        {
            total: $scope.questionSets.length,
            getData: function ($defer, params) {

                var filteredData = params.filter() ?
                    $filter('filter')($scope.questionSets, params.filter()) : $scope.questionSets;

                var orderedData = params.sorting() ?
                    $filter('orderBy')(filteredData, params.orderBy()) : filteredData;

                params.total(orderedData.length);

                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));

            }
        });

        $scope.tableParams.settings().$scope = $scope;

        $scope.createQuestionSet = function () {
            $location.path('/questions/create')
        }

        $scope.editQuestionSet = function (questionId) {
            $location.path('questions/edit/' + questionId);
        }

        $scope.previewQuestionSet = function (questionId) {
            $location.path('question/preview/' + questionId);

        }

     
    }]);