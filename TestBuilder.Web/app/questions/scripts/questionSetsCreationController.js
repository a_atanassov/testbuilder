﻿testBuilderApp.controller('questionSetsCreationController', ['$scope', 'questionsService', '$location', 'facultiesServices', 'ngDialog', '$routeParams'
    , function questionsController($scope, questionsService, $location, facultiesServices, ngDialog, $routeParams) {
        'use strict';
        $scope.questionSet = { subject: {}, questions: new Array() };
        $scope.subjects = [];
        $scope.faculties = [];

        $scope.facultySubjectSpecificQuestions = [];

        var questionId = undefined;
        var params = $routeParams;
        questionId = params.questionId;

        var init = function () {
            facultiesServices.getAllFaculties().then(function (data) {
                $scope.faculties = data;
                questionsService.getAllSubjects().then(function (data) {
                    $scope.subjects = data;

                    if (questionId != undefined) {
                        questionsService.getExistingQuestionSet(questionId).then(function (data) {
                            $scope.questionSet = data;
                            $scope.subjects.forEach(function (subject) {
                                if (subject.id == $scope.questionSet.subjectId) {
                                    $scope.questionSet.subject = subject;
                                }
                            });
                            $scope.faculties.forEach(function (faculty) {
                                if (faculty.id == $scope.questionSet.facultyId) {
                                    $scope.selectedFaculty = faculty;
                                }
                            });
                            getExistingQuestions();

                        })
                    }
                })
            });
        }

        init();

        $scope.createQuestion = function (questionSetsQuestions) {
            var dialogObj = {
                template: 'app/questions/views/createQuestionTemplate.html',
                className: 'ngdialog-theme-default ngdialog-theme-wide-dialog',
                scope: $scope,
                closeByDocument: false,
                controller: ['$scope', function ($scope, questionSetsQuestions) {
                    $scope.question = { typeId: {}, answers: [] };
                    var parentscope = $scope.$parent;
                    var sets = parentscope.questionSet.questions;
                    $scope.newAnswer = { name: '', isCorrect: false, };
                    $scope.selectTypeOfQuestion = function (typeId) {
                        $scope.question.typeId = typeId;
                    }

                    $scope.checkNewAnswer = function () {
                        $scope.newAnswer.isCorrect = !$scope.newAnswer.isCorrect;
                    }

                    $scope.addAnswer = function (question) {
                        var questionToAdd = angular.copy(question);
                        $scope.question.answers.push(questionToAdd);
                    }

                    $scope.addQuestion = function () {
                        sets.push($scope.question);
                        $scope.newAnswer = { name: '', isCorrect: false };

                    }

                    $scope.removeAnswer = function (answer) {
                        var indexOfObjectToRemove = $scope.question.answers.indexOf(answer);
                        $scope.question.answers.splice(indexOfObjectToRemove, 1);

                    }

                }]
            }
            ngDialog.open(dialogObj);
        }

        $scope.setQuestionSetsSubject = function (subject) {
            $scope.questionSet.subject = subject;
            if ($scope.selectedFaculty.id) {
                getExistingQuestions();
            }
        }

        $scope.addExistingQuestion = function (question) {
            var questionToAdd = angular.copy(question);
            $scope.questionSet.questions.push(questionToAdd);
        }

        $scope.selectFaculty = function (faculty) {
            $scope.selectedFaculty = faculty;
            if ($scope.questionSet.subject.id) {
                getExistingQuestions();
            }
        }

        $scope.createQuestionSet = function () {
            $scope.questionSet.subjectId = $scope.questionSet.subject.id;
            $scope.questionSet.facultyId = $scope.selectedFaculty.id;
            if (questionId != undefined) {
                questionsService.updateQuestionSet($scope.questionSet).then(function () {
                    $location.path('questions');
                });
            }
            else {
                questionsService.createQuestionSet($scope.questionSet).then(function () {
                    $location.path('questions');
                });
            }

        }

        var getExistingQuestions = function () {
            questionsService.getExistingQuestions($scope.selectedFaculty.id, $scope.questionSet.subject.id).then(function (data) {
                $scope.facultySubjectSpecificQuestions = data;
            })
        }

        $scope.onDropComplete = function (index, obj, evt) {
            var otherObj = $scope.questionSet.questions[index];
            var otherIndex = $scope.questionSet.questions.indexOf(obj);
            obj.index = index;
            otherObj.index = otherIndex;
            $scope.questionSet.questions[index] = obj;
            $scope.questionSet.questions[otherIndex] = otherObj;
        };
     

    }]);