﻿var testBuilderApp = angular.module('testBuilderApp', ['ngResource', 'ngRoute', 'ngTable', 'ngDialog', 'ngDraggable'])
    .config(function ($routeProvider) {
        $routeProvider.when('/createStudent',
            {
                templateUrl: 'app/templates/studentsTemplate.html',
                controller: 'studentsController'
            })
            .when('/',
            {
                templateUrl: 'app/templates/homeTemplate.html',
                controller: 'studentsController'
            })
            .when('/students',
            {
                templateUrl: 'app/templates/studentsTable.html',
                controller: 'studentsController'
            })
            .when('/faculties',
            {
                templateUrl: 'app/faculties/views/manageFaculties.html',
                controller: 'facultiesController'
            })
            .when('/questions',
            {
                templateUrl: 'app/questions/views/manageQuestions.html',
                controller: 'questionsController'
            })
            .when('/questions/create',
            {
                templateUrl: 'app/questions/views/createQuestionSet.html',
                controller: 'questionSetsCreationController'
            })
            .when('/question/preview/:questionSetId',
            {
                templateUrl: 'app/questions/views/questionPreview.html',
                controller: 'questionPreviewController'
            })
            .when('/questions/edit/:questionId',
            {
                templateUrl: 'app/questions/views/createQuestionSet.html',
                controller: 'questionSetsCreationController'

            })

        
    });