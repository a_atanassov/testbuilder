﻿testBuilderApp.factory('facultiesServices', ['$http', '$q', 'productResource', function ($http, $q, productResource) {
    var serviceBase = productResource.serviceBase;

    var facilityFactory = {};



    var _getAllFaculties = function () {
        var resourceURL = serviceBase + 'api/faculty/getAllFaculties';
        var promise = productResource.defferedResourceGet(resourceURL);

        return promise;
    }

    var _createFaculty = function (faculty) {
        var resourceURL = serviceBase + 'api/faculty/createFaculty';
        var promise = productResource.defferedResourcePost(resourceURL, faculty);

        return promise;
    }

    facilityFactory.getAllFaculties = _getAllFaculties;
    facilityFactory.createFaculty = _createFaculty;


    return facilityFactory;


}])

