﻿testBuilderApp.controller('facultiesController', ['$scope','ngDialog', 'facultiesServices', '$filter', 'ngTableParams',
    function facultiesController($scope, ngDialog, facultiesServices, $filter, ngTableParams) {

        $scope.searchFaculties = '';

        $scope.createFaculty = function () {

            var dialogObj = {
                template: 'app/faculties/views/createFacultyTemplate.html',
                className: 'ngdialog-theme-default',
                scope: $scope,
                closeByDocument: false,
                
            }
            ngDialog.open(dialogObj);
        }

        $scope.faculties = [];

        var init = function () {
            facultiesServices.getAllFaculties().then(function (faculties) {
                $scope.faculties = faculties;
            })
        }

        init();


        $scope.$watch("searchFaculties", function () {
            $scope.tableParams.filter().name = $scope.searchFaculties;
            $scope.tableParams.reload();
            $scope.tableParams.page(1);
        });

        $scope.$watch("faculties", function () {
            $scope.tableParams.reload();
            $scope.tableParams.page(1);
        }, true);

        $scope.tableParams = new ngTableParams({
            page: 1,
            total: $scope.faculties.length,
            count: 10,
            filter: {
                name: $scope.searchFaculties,

            },
            sorting: { name: 'asc' }
        },
        {
            total: $scope.faculties.length,
            getData: function ($defer, params) {

                var filteredData = params.filter() ?
                    $filter('filter')($scope.faculties, params.filter()) : $scope.faculties;

                var orderedData = params.sorting() ?
                    $filter('orderBy')(filteredData, params.orderBy()) : filteredData;

                params.total(orderedData.length);

                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));

            }
        });

        $scope.tableParams.settings().$scope = $scope;


        $scope.saveFaculty = function (faculty) {
            facultiesServices.createFaculty(faculty).then(function () {
                facultiesServices.getAllFaculties().then(function (faculties) {
                    $scope.faculties = faculties;
                });
            });
        }
    }]);