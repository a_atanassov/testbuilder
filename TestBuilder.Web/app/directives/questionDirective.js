﻿testBuilderApp.directive('questionDirective', [function () {
    'use strict';
    return {
        restrict: 'E',
        transclude: true,
        replace: true,
        scope: {
            inputquestion: '=',
            questions: '='
        },
        templateUrl: 'app/directives/templates/questionDirectiveTemplate.html',
        controller: ['$scope', 'ngDialog', function ($scope, ngDialog) {
            $scope.removeQuestion = function (question) {
                var elementIndex = $scope.questions.indexOf(question);
                $scope.questions.splice(elementIndex, 1);
            }

            $scope.editQuestion = function (question) {
                var dialogObj = {
                    template: 'app/questions/views/createQuestionTemplate.html',
                    className: 'ngdialog-theme-default ngdialog-theme-wide-dialog',
                    scope: $scope,
                    closeByDocument: false,
                    controller: ['$scope', function ($scope, questionSetsQuestions) {
                        $scope.question = angular.copy($scope.inputquestion);
                       
                        $scope.newAnswer = { name: '', isCorrect: false, };
                        $scope.selectTypeOfQuestion = function (typeId) {
                            $scope.question.typeId = typeId;
                        }

                        $scope.selectTypeOfQuestion($scope.question.typeId);

                        $scope.checkNewAnswer = function () {
                            $scope.newAnswer.isCorrect = !$scope.newAnswer.isCorrect;
                        }

                        $scope.addAnswer = function (question) {
                            var questionToAdd = angular.copy(question);
                            $scope.question.answers.push(questionToAdd);
                        }

                        $scope.addQuestion = function () {
                            //question = angular.copy($scope.question);
                            var indexOfQuestion = $scope.questions.indexOf($scope.inputquestion);
                            $scope.questions[indexOfQuestion] = angular.copy($scope.question);
                            //elementInArray = $scope.question;

                        }

                        $scope.removeAnswer = function (answer) {
                            var indexOfObjectToRemove = $scope.question.answers.indexOf(answer);
                            $scope.question.answers.splice(indexOfObjectToRemove, 1);

                        }

                    }]
                }
                ngDialog.open(dialogObj);
            }
        }]

    };

}]);