﻿testBuilderApp.controller('userManagementController', ['$scope', 'ngDialog',
    function userManagementController($scope, ngDialog) {
        $scope.user = { name: '', password: '' };

        $scope.isUserLoggedIn = false;

        $scope.logIn = function () {
            $scope.isUserLoggedIn = true;

        }

        $scope.register = function () {
            var dialogObj = {
                template: 'app/userAuthentication/views/registerForm.html',
                className: 'ngdialog-theme-default',
                scope: $scope,
                closeByDocument: false,
                //controller: ['$scope', function ($scope) {
                //    $scope.todelete = element;

                //}]
            }
            ngDialog.open(dialogObj);
        }
    }]);